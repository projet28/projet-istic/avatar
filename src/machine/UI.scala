package machine

import scala.swing._
import java.awt.Color

/**
 * MainFrame realizing the CopyThat application
 */
class UI extends MainFrame {
  
  // Propriétés de la fenêtre
  title = "chat avatar"
  preferredSize = new Dimension(250, 500)
  
  // Quelques champs définissant les composants
  val input = new InField
  val output = new ResultText
  val copy = new CopyButton("chat",input,output)
  
  
  // Ajout des composants à la fenêtre
  contents = new BoxPanel(Orientation.Vertical) {
    background = Color.BLACK
    
    
    contents += output
    contents += input
    contents += copy
  }
    
}