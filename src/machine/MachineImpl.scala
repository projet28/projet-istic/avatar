package machine
import scala.io.Source
import scala.collection.mutable.ListBuffer
object MachineImpl extends MachineDialogue{

	/*
	 * 
	 * BASE DE DONNEE
	 * 
	 */

	val repMairie:String= "L'adresse de Mairie de Rennes est : Place de la Mairie"
			val repGare:String= "L'adresse de Gare SNCF est : 19, Place de la Gare"
			val repPaillette:String= "L'adresse de Théâtre La Paillette est : 2, Rue du Pré de Bris"
			val repTNB:String= "L'adresse de Théâtre National de Bretagne est : 1, Rue Saint-Hélier"

			val donnee:Map[String,String]= Map(("bonjour", "bonjour"),
					("salut", "salut"),
					("bonsoir", "bonsoir"),
					("hey", "hey"),
					("mairie", repMairie),
					("hôtel de ville", repMairie),
					("hotel de ville", repMairie),
					("gare", repGare),
					("sncf", repGare),
					("paillette", repPaillette),
					("théâtre la paillette", repPaillette),
					("theatre la paillette", repPaillette),
					("théâtre national de bretagne", repTNB),
					("theatre national de bretagne", repTNB),
					("théâtre de bretagne", repTNB),
					("theatre de bretagne", repTNB),
					("tnb", repTNB))

			/*
			 * 
			 * ACCEPTATION ERREUR	
			 *                             
			 */

			def erreur(s:String):String={
					var str: String = "Je ne comprends pas votre demande"
							for(i <- donnee){

							}
str
}                                    

/*
 * 
 * RECHERCHE MOT CLEF
 *                                     
 */
def etude(s:String):String={
		var str: String = "Je ne comprends pas votre demande"
				for(i <- donnee){
					if(s.toLowerCase().contains(i._1)){
						str = i._2
					}
				}
str
}



//#####################################################################//
//#####################################################################//
//###################    V2 - Base de donnee TXT    ###################//
//#####################################################################//
//#####################################################################//





//#####################################################//
//################    FONCTION  ASK    ################//
//#####################################################//

//----------------- Fonction Principal ----------------//

//Ask
/** envoi d'une requête à la machine et récupération de sa réponse
 *  
 *  @param s la requête
 *  @result la liste de réponses
 */
def ask(s:String):List[String]={


		resultatAvatar(stringtolist(s))
}


//stringtolist                                                                 //OK //
/** Prend une chaine de caracthere et place chaqu'un des mot dans une liste
 *  
 *  @param s la chaine de caracthere de l'utilisateur
 *  @result la liste des mots dans la chaine
 */
def stringtolist(s:String):List[String]={
		//changement du String en Liste de String

		//var tmp = ""
		//var list = List("")
		var tmp = "";
		var list = new ListBuffer[String]()


				for(i<- intWrapper(0) to s.length-1){
					if(s.charAt(i)!= ' '){
						tmp = tmp+s.charAt(i)
					}else{
						list+=tmp
								tmp = ""
					}
				}
		list+=tmp
				val listResult = list.toList
				listResult
}



//--------- Fonctions des reponses de l'avatar --------//

//ResultatAvatar
/**
 * Fonction qui renvoie la liste de String qui represente les reponses de l'avatar, les reponses sont faites en 
 * fonction de la liste de string de depart qui est constitué de la requete de l'utilisateur
 * 
 * @param liste de String constitué des mots rentré par l'utilisateur
 * @result liste contenant la reponse de l'avatar
 */
def resultatAvatar(l:List[String]):List[String]={
		//var res = List[String]()
				var res2 = new ListBuffer[String]()
		/*l match{
				case a::b => if(rechercheMap2(a)!=""){res2+=(rechercheMap2(a));resultatAvatar2(b)}else{resultatAvatar2(b)}
				case Nil => if(isEmpty2(res2.toList)==true){res2+=(zero)}
		}*/
		
		val listResult = resultatAvatarRec(l,res2).toList
				listResult
				//res
}


def resultatAvatarRec(l:List[String],list:ListBuffer[String]):ListBuffer[String]={
  l match{
				case a::b => if(rechercheMap(a)!=""){list+=(rechercheMap(a));resultatAvatarRec(b,list)}else{resultatAvatarRec(b,list)}
				case Nil => if(isEmpty(list.toList)==true){list+=("Je ne comprends pas votre demande")}
		}
  list
}


//IsEmpty                                                                                   //OK//
/**
 * Verifie que la liste donner en parametre est vide
 * 
 * @param liste de String des reponse de l'avatar
 * @result Boolean montrant si la liste est vide ou non
 */
def isEmpty (l:List[String]):Boolean={
		l match{
		case List("")=> true
		case List() => true
		case Nil => true
		case _ => false
		}
}



//----------- Transformation du Doc en Map -----------//

//docToMap
/**
 * A partir du document, une map est cree pour contenir la base de donnée
 * 
 * @result la map de l'assotation des mot que pourrais rentrer l'utilisateur et des reponce que l'avatar doit rendre
 */
def docToMap:Map[String,String]={
		var mapDoc = Map[String, String]()
				val doc = "doc/DonneesInitiales.txt"
				var tmp = List()
						for (line <- Source.fromFile(doc).getLines) {

									mapDoc += (treatLine(line)(0) -> treatLine(line)(1))
						}
				mapDoc
}


//TreatLine
/**
 * Fonction permetant de traiter une line de la base de donner de la forme (String;String)
 * 
 * @param une line de la base de donnée
 * @result renvoie les 2 elements auparavent separer par un point virgule , en les mettant dans un tableau a 2 case pour simplifier le traitement
 */
def treatLine (l:String):List[String]={
		var tmp =""
				var list = new ListBuffer[String]()
						for(i<- intWrapper(0) to l.length-1){
							if(l.charAt(i)!= ';'){
								tmp = tmp+l.charAt(i)
							}else{
								list+=tmp
								tmp = ""
							}
						}
				list+=tmp
				val listResult = list.toList
				listResult
}


/*def stringtolist2(s:String):List[String]={
		//changement du String en Liste de String

		//var tmp = ""
		//var list = List("")
		var tmp = "";
		var list = new ListBuffer[String]()


				for(i<- intWrapper(0) to s.length-1){
					if(s.charAt(i)!= ' '){
						tmp = tmp+s.charAt(i)
					}else{
						list+=tmp
								tmp = ""
					}
				}
		list+=tmp
				val listResult = list.toList
				listResult
}*/




//---- Traitement de la requete avec la base de donnée ----//

//Recherche
/**
 * Recherche d'un mot d'en la Map qui contient la base de donnée, pour construire la reponse de l'avatar
 * 
 * @param String un mot rentrer par l'utilisateur que l'on recherche dans la base de donnée
 * @result String qui contient la reponse de l'avatar
 */
def rechercheMap (s:String):String={
				docToMap.getOrElse(s,"")
				/* var res = ""
	  for(i <- 0 to mapDoc.size){
	    if(mapDoc(i,0)==s){
	      res = mapDoc(i,1)
	    }
	  }
	  res*/
}








//#####################################################################//
//#####################################################################//
//###################    V1 - Base de donnee MAP    ###################//
//#####################################################################//
//#####################################################################//




//#####################################################//
//################    FONCTION  ASK    ################//
//#####################################################//

//----------------- Fonction Principal ----------------//

//Ask
/** envoi d'une requête à la machine et récupération de sa réponse
 *  
 *  @param s la requête
 *  @result la liste de réponses
 */
def ask2(s:String):List[String]={


		resultatAvatar2(stringtolist2(s))
}


//stringtolist                                                                 //OK //
/** Prend une chaine de caracthere et place chaqu'un des mot dans une liste
 *  
 *  @param s la chaine de caracthere de l'utilisateur
 *  @result la liste des mots dans la chaine
 */
def stringtolist2(s:String):List[String]={
		//changement du String en Liste de String

		//var tmp = ""
		//var list = List("")
		var tmp = "";
		var list = new ListBuffer[String]()


				for(i<- intWrapper(0) to s.length-1){
					if(s.charAt(i)!= ' '){
						tmp = tmp+s.charAt(i)
					}else{
						list+=tmp
								tmp = ""
					}
				}
		list+=tmp
				val listResult = list.toList
				listResult
}



//--------- Fonctions des reponses de l'avatar --------//

//ResultatAvatar
/**
 * Fonction qui renvoie la liste de String qui represente les reponses de l'avatar, les reponses sont faites en 
 * fonction de la liste de string de depart qui est constitué de la requete de l'utilisateur
 * 
 * @param liste de String constitué des mots rentré par l'utilisateur
 * @result liste contenant la reponse de l'avatar
 */
def resultatAvatar2(l:List[String]):List[String]={
		//var res = List[String]()
				var res2 = new ListBuffer[String]()
		/*l match{
				case a::b => if(rechercheMap2(a)!=""){res2+=(rechercheMap2(a));resultatAvatar2(b)}else{resultatAvatar2(b)}
				case Nil => if(isEmpty2(res2.toList)==true){res2+=(zero)}
		}*/
		
		val listResult = resultatAvatar2Rec(l,res2).toList
				listResult
				//res
}


def resultatAvatar2Rec(l:List[String],list:ListBuffer[String]):ListBuffer[String]={
  l match{
				case a::b => if(rechercheMap2(a)!=""){list+=(rechercheMap2(a));resultatAvatar2Rec(b,list)}else{resultatAvatar2Rec(b,list)}
				case Nil => if(isEmpty2(list.toList)==true){list+=("Je ne comprends pas votre demande")}
		}
  list
}


//IsEmpty                                                                                   //OK//
/**
 * Verifie que la liste donner en parametre est vide
 * 
 * @param liste de String des reponse de l'avatar
 * @result Boolean montrant si la liste est vide ou non
 */
def isEmpty2 (l:List[String]):Boolean={
		l match{
		case List("")=> true
		case List() => true
		case Nil => true
		case _ => false
		}
}




//---- Traitement de la requete avec la base de donnée ----//

//Recherche                                                                              //OK//
/**
 * Recherche d'un mot d'en la Map qui contient la base de donnée, pour construire la reponse de l'avatar
 * 
 * @param String un mot rentrer par l'utilisateur que l'on recherche dans la base de donnée
 * @result String qui contient la reponse de l'avatar
 */
def rechercheMap2 (s:String):String={
		donnee.getOrElse(s,"")
		/* var res = ""
	  for(i <- 0 to mapDoc.size){
	    if(mapDoc(i,0)==s){
	      res = mapDoc(i,1)
	    }
	  }
	  res*/
}



//#################################################//
//#################################################//



// Pour la partie test par le client
//TODO
def reinit= {

}



def test(l:List[String]):List[String]= {
		resultatAvatar(l)

}
}