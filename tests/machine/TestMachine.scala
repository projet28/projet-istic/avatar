package machine
import org.junit.Test
import org.junit.Assert._
import scala.collection.mutable.ListBuffer

class TestIntegration {

	// initialisation des objets sous test
	val m= MachineImpl
			m.reinit

			/*
  @Test
  def test1_1{    
    assertEquals(List("L'adresse de Mairie de Rennes est : Place de la Mairie"),m.test(List("Où est donc la Mairie de Rennes?")))
  }*/


			//#####################################################//
			//##################    TEST  ASK    ##################//
			//#####################################################//

			//#####################  V1  #####################//

			//----------------- ask Test ----------------//


			@Test
			def asktest1{
		assertEquals(m.ask2("salut"),List("salut"))
	}

	@Test
	def asktest2{
		assertEquals(m.ask2("sncf"),List("L'adresse de Gare SNCF est : 19, Place de la Gare"))
	}

	@Test
	def asktest3{
		assertEquals(m.ask2("je cherche de la gare"),List("L'adresse de Gare SNCF est : 19, Place de la Gare"))
	}
	@Test
	def asktest4{
		assertEquals(m.ask2("bonjour , je cherche de la gare"),List("bonjour","L'adresse de Gare SNCF est : 19, Place de la Gare"))
	}
	@Test
	def asktest5{
		assertEquals(m.ask2("bonjour , je cherche de la gare et la mairie"),List("bonjour","L'adresse de Gare SNCF est : 19, Place de la Gare","L'adresse de Mairie de Rennes est : Place de la Mairie"))
	}


	//----------------- stringtolist Test ----------------//
	@Test
	def stringtolisttest1{
		assertEquals(m.stringtolist2(""),List(""))
	}

	@Test
	def stringtolisttest2{
		assertEquals(m.stringtolist2("Bonjours"),List("Bonjours"))
	}

	@Test
	def stringtolisttest3{
		assertEquals(m.stringtolist2("Hello World"),List("Hello","World"))
	}

	@Test
	def stringtolisttest4{
		assertEquals(m.stringtolist2("Hello World !!!"),List("Hello","World","!!!"))
	}


	//----------------- resultatAvatar2 Test ----------------//


	@Test
	def resultatAvatar2test1{
		assertEquals(m.resultatAvatar2(List("salut","a")),List("salut"))
	}

	@Test
	def resultatAvatar2test2{
		assertEquals(m.resultatAvatar2(List()),List("Je ne comprends pas votre demande"))
	}

	@Test
	def resultatAvatar2test3{
		assertEquals(m.resultatAvatar2(List("salut","hey","bonsoir")),List("salut","hey","bonsoir"))
	}



	//----------------- isEmpty Test ----------------//

	@Test
	def isemptytest1{
		assertTrue(m.isEmpty2(List("")))
	}

	@Test
	def isemptytest2{
		assertTrue(m.isEmpty2(List()))
	}

	@Test
	def isemptytest3{
		assertFalse(m.isEmpty2(List("a")))
	}

	@Test
	def isemptytest4{
		assertFalse(m.isEmpty2(List("a","b","c")))
	}


	@Test
	def isemptytest5{
		assertTrue(m.isEmpty2(List()))
	}

	@Test
	def isemptytest6{
		var res2 = new ListBuffer[String]()
				assertTrue(m.isEmpty(res2.toList))
	}
	//----------------- rechercheMap2 Test ----------------//

	@Test
	def rechercheMaptest1{
		assertEquals(m.rechercheMap2("hey"),"hey")
	}

	@Test
	def rechercheMaptest2{
		assertEquals(m.rechercheMap2("mairie"),"L'adresse de Mairie de Rennes est : Place de la Mairie")
	}




	//#####################  V2  #####################//


	//----------------- ask Test ----------------//


	@Test
	def askV2test1{
		assertEquals(m.ask("Salut"),List("Salut"))
	}
	
		@Test
	def askV2test2{
		assertEquals(m.ask("Salut , je cherche la Mairie , Au revoir"),List("Salut","Place de la Mairie","Au revoir"))
	}

		//----------------- doctomap Test ----------------//
	/*@Test
	def doctomapV2test2{
		assertEquals(m.docToMap,Map(ArrowAssoc("Mairie de Rennes")->"Place de la Mairie"))
	}
*/

	//----------------- treatLine Test ----------------//


	@Test
	def treatLineV2test1{
		assertEquals(m.treatLine("a;b"),List("a","b"))
	}

	@Test
	def treatLinetest2{
		assertEquals(m.treatLine("théâtre la paillette;L'adresse de Théâtre La Paillette est : 2, Rue du Pré de Bris"),List("théâtre la paillette","L'adresse de Théâtre La Paillette est : 2, Rue du Pré de Bris"))
	}
	
	



}